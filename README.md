## __Demo project:__
Create server and deploy application on DigitalOcean

## __Technologies used:__
DigitalOcean, Linux, Java, Gradle

## __Project Description:__
* Setup and configure a server on DigitalOcean
* Create and configure a new Linux user on the Droplet (Security best practice)
* Deploy and run a Java Gradle application on Droplet

## __Description in details:__
### __Setup and configure a server on DigitalOcean__
__Step 1:__ Go on DO and create Ubuntu Droplet with your nessesary configuratuions (Image, Plan, Region)

__Step 2:__ Create SSH key for your droplet  

__Step 3:__ Create firewall and open SSH port = 22, and assign it on your droplet

### __Create and configure a new Linux user on the Droplet (Security best practice)__
__Step 1:__ Connect to your droplet via SSH (cope the adress from DO)
```sh
ssh root@Your_Droplet_Ip_here
```
__Step 2:__
```sh
apt update
```
__Step 3:__ Install java
```sh
apt install openjdk-8-jre-headless
```  
__Step 4:__ Create Linux User for Droplet 
```sh
adduser "Your_user_name"  
```
__Step 5:__ Add user to root group 
```sh
usermod -aG sudo "Your_user_name"
```
__Step 6:__ Switch user
```sh
su - "Your_user_name"
```
__Step 7:__ Add SSH key  to created Linux user:
```sh
mkdir .ssh
```
__Step 8:__ Create file __autorized_keys__ and paste youe key here:
```sh
sudo vim  .ssh/autorized_keys
```

__Step 9:__ Now You can connect directly to Your user via SSH  

### __Deploy and run a Java Gradle application on Droplet__
__Step 1:__ Clone app from [here](https://github.com/bbachi/react-nodejs-example) and enter this folder  

__Step 2:__ Build image 
```sh
./gradlew build
```
__Step 3:__ Copy image to your droplet 
```sh
scp build/libs/java-react-example.jar "your_user_name"@164.92.253.8:/home/"your_user_name"
```	
__Step 4:__ Run a Java Graddle app 
```sh
java -jar java-react-example.jar 
```
__Step 5:__ Open port 7071 on Your droplet for app  


__Step 6:__ Check  app in Your browser


